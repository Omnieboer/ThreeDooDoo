import org.jetbrains.compose.compose
import org.jetbrains.compose.desktop.application.dsl.TargetFormat

plugins {
	kotlin("multiplatform")
	id("org.jetbrains.compose")
}

group = "dyn.omni"
version = "1.0.3"

repositories {
	google()
	mavenCentral()
	maven("https://maven.pkg.jetbrains.space/public/p/compose/dev")
}

kotlin {
	jvm {
		compilations.all {
			kotlinOptions.jvmTarget = "11"
		}
		withJava()
	}
	sourceSets {
		val jvmMain by getting {
			dependencies {
				implementation(compose.desktop.currentOs)
				implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.3.2")
				implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.6.2")
			}
		}
		val jvmTest by getting
	}
}

compose.desktop {
	application {
		mainClass = "MainKt"
		nativeDistributions {
			targetFormats(TargetFormat.Dmg, TargetFormat.Msi, TargetFormat.Deb)
			packageName = "ThreeDooDoo"
			packageVersion = project.version.toString()
			description = "It's like a TwoDoo, but OneDoo better"
			copyright = "© 2022 Omnieboer. All rights reserved."
			windows {
				iconFile.set(project.file("icon.ico"))
				perUserInstall = true
				shortcut = true
				menu = true
			}
		}
	}
}
