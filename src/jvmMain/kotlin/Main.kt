// Copyright 2000-2021 JetBrains s.r.o. and contributors. Use of this source code is governed by the Apache 2.0 license that can be found in the LICENSE file.
import androidx.compose.desktop.ui.tooling.preview.Preview
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.twotone.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.application
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import view.theme.*
import java.nio.file.Path

@ExperimentalComposeUiApi
@Composable
@Preview
fun app() {
	val themes = listOf(damenBlue(), damenLight(), damenWhite(), damenIllegal())
	var themeIndex by remember { mutableStateOf(themes.lastIndex) }
	val json = Json {
		encodeDefaults = true
		prettyPrint = true
		isLenient = true
	}
	var editMode by remember { mutableStateOf(false) }
	val toDoItems = mutableStateListOf<String>()
	var chosenIndex by remember { mutableStateOf(0) }
	val toDoSave = Path.of(System.getenv("APPDATA"), "ThreeDooDoo", "List.json").toFile()
	if (!toDoSave.createNewFile()) {
		toDoItems.addAll(
			json.decodeFromString<MutableList<String>>(toDoSave.readText())
				.filter { it.isNotBlank() }
		)
	}
	fun save() {
		toDoSave.writeText(json.encodeToString(toDoItems.toMutableList()))
	}
	MaterialTheme(themes[themeIndex]) {
		Surface(
			modifier = Modifier.fillMaxSize()
		) {
			Column(
				modifier = Modifier.fillMaxSize(),
				verticalArrangement = Arrangement.SpaceEvenly,
				horizontalAlignment = Alignment.CenterHorizontally
			) {
				Row(
					verticalAlignment = Alignment.CenterVertically,
					horizontalArrangement = Arrangement.Center
				) {
					IconButton(
						onClick = {
							themeIndex = (themeIndex + 1) % themes.size
						}
					) {
						Icon(Icons.TwoTone.Settings, null)
					}
					IconButton(onClick = {
						editMode = !editMode
					}) {
						Icon(
							Icons.TwoTone.Edit, null
						)
					}
				}
				if (editMode) {
					LazyColumn(
						modifier = Modifier.weight(11f)
					) {
						itemsIndexed(toDoItems) { index, it ->
							Row(
								modifier = Modifier.fillMaxWidth(),
								verticalAlignment = Alignment.CenterVertically,
								horizontalArrangement = Arrangement.SpaceEvenly
							) {
								var localItem by remember { mutableStateOf(it) }
								OutlinedTextField(
									modifier = Modifier.fillMaxWidth().padding(horizontal = 16.dp, vertical = 4.dp),
									value = localItem,
									onValueChange = {
										localItem = it
										toDoItems[index] = it
										save()
									},
									trailingIcon = {
										IconButton(onClick = { toDoItems.remove(it) }) {
											Icon(Icons.TwoTone.Minus, null)
										}
									}
								)
							}
						}
						item {
							var newItem by remember { mutableStateOf("") }
							OutlinedTextField(
								modifier = Modifier.fillMaxWidth().padding(horizontal = 16.dp, vertical = 4.dp),
								value = newItem,
								onValueChange = {
									if (it.endsWith("\n") || it.endsWith("\r")) {
										toDoItems.add(newItem)
									} else {
										newItem = it
									}
									save()
								},
								label = {
									Text("New Item")
								},
								trailingIcon = {
									IconButton(
										onClick = {
											toDoItems.add(newItem)
											save()
										}
									) {
										Icon(Icons.TwoTone.Add, null)
									}
								}
							)
						}
					}
				} else {
					Column(
						modifier = Modifier.fillMaxWidth().weight(11f),
						verticalArrangement = Arrangement.SpaceEvenly,
						horizontalAlignment = Alignment.CenterHorizontally
					) {
						Row(
							modifier = Modifier.fillMaxWidth().weight(11f),
							verticalAlignment = Alignment.CenterVertically,
							horizontalArrangement = Arrangement.SpaceEvenly
						) {
							IconButton(
								modifier = Modifier.weight(1f).aspectRatio(1f),
								onClick = {
									if (chosenIndex > 0) {
										chosenIndex--
									}
								}) {
								Icon(
									Icons.TwoTone.KeyboardArrowLeft, null
								)
							}

							Text(
								modifier = Modifier.weight(6f),
								text = if (toDoItems.isNotEmpty()) {
									toDoItems[chosenIndex]
								} else {
									"List is Empty"
								},
								style = MaterialTheme.typography.h1,
								textAlign = TextAlign.Center,
							)

							IconButton(
								modifier = Modifier.weight(1f).aspectRatio(1f),
								onClick = {
									if (chosenIndex < toDoItems.lastIndex) {
										chosenIndex++
									}
								}) {
								Icon(
									Icons.TwoTone.KeyboardArrowRight, null
								)
							}
						}

						IconButton(
							onClick = {
								if (chosenIndex > 0) {
									chosenIndex--
								}
								if (toDoItems.size <= 1) {
									toDoItems.clear()
								} else {
									toDoItems.removeAt(chosenIndex)
								}
								save()
							}
						) {
							Icon(Icons.TwoTone.Check, null)
						}
					}
				}
			}
		}
	}
}

@ExperimentalComposeUiApi
fun main() = application {
	Window(
		title = "ThreeDooDoo, it's the next step of TwoDoo",
		onCloseRequest = ::exitApplication
	) {
		app()
	}
}
