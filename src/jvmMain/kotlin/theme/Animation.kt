package view.theme

import androidx.compose.animation.core.CubicBezierEasing

val SlowInSlowOutEasing = CubicBezierEasing(0.42f, 0f, 0.58f, 1f)
val FastInSlowOutEasing = CubicBezierEasing(0.25f, 0.1f, 0.25f, 1f)
val FastInFastOutEasing = CubicBezierEasing(0.25f, 0.1f, 0.75f, 0.9f)