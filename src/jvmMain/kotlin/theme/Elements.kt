package view.theme

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.twotone.Delete
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.loadSvgPainter
import androidx.compose.ui.res.useResource
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

@Composable
fun erasableTextField(
	modifier: Modifier = Modifier,
	text: String,
	onValueChange: (String) -> Unit = {},
	label: String = "",
	enabled: Boolean = true,
) {
	var local by remember { mutableStateOf(text) }
	OutlinedTextField(
		modifier = modifier.padding(5.dp),
		enabled = enabled,
		value = local,
		onValueChange = {
			local = it
			onValueChange(local)
		},
		label = { Text(text = label) },
		trailingIcon = {
			IconButton(
				modifier = Modifier,
				onClick = {
					local = ""
					onValueChange(local)
				}
			){
				Icon(
					imageVector = Icons.TwoTone.Delete,
					contentDescription = null,
				)
			}
		}
	)
}

@Composable
fun verticalDivider(
	modifier: Modifier = Modifier,
	color: Color = MaterialTheme.colors.onSurface.copy(alpha = DividerAlpha),
	thickness: Dp = 1.dp,
) {
	Box(
		modifier.fillMaxHeight().width(thickness).background(color = color)
	)
}

private const val DividerAlpha = 0.12f

@Composable
fun loadSvgPainter(resourcePath: String, density: Density = LocalDensity.current): Painter =
	useResource(resourcePath) { loadSvgPainter(it, density) }

