package view.theme

import androidx.compose.ui.graphics.Color

private val DamenSilver100 = Color(0xFFF8FAFB)
private val DamenSilver200 = Color(0xFFF2F6F8)
private val DamenSilver300 = Color(0xFFE4E8EB)
private val DamenSilver400 = Color(0xFFD1D5DA)
private val DamenSilver500 = Color(0xFFB3B9C2)
private val DamenSilver600 = Color(0xFF99A2AF)
private val DamenSilver700 = Color(0xFF808B9B)
private val DamenBlue100 = Color(0xFFC2C9EB)
private val DamenBlue200 = Color(0xFF9AA6DD)
private val DamenBlue300 = Color(0xFF7183CF)
private val DamenBlue400 = Color(0xFF5068C5)
private val DamenBlue500 = Color(0xFF2546B1)
private val DamenBlue600 = Color(0xFF193CA5)
private val DamenBlue700 = Color(0xFF001F85)
private val DamenOrange100 = Color(0xFFFED1C0)
private val DamenOrange200 = Color(0xFFFEB29A)
private val DamenOrange300 = Color(0xFFFF9371)
private val DamenOrange400 = Color(0xFFFF7C52)
private val DamenOrange500 = Color(0xFFFA7247)
private val DamenOrange600 = Color(0xFFF4673B)
private val DamenOrange700 = Color(0xFFEA5E31)
private val DamenLightBlue100 = Color(0xFFF6FBFC)
private val DamenLightBlue200 = Color(0xFFEAF5F8)
private val DamenLightBlue300 = Color(0xFFD6EBF1)
private val DamenLightBlue400 = Color(0xFF97CDDD)
private val DamenLightBlue500 = Color(0xFF4AACC7)
private val DamenLightBlue600 = Color(0xFF008CB3)
private val DamenLightBlue700 = Color(0xFF0079AA)
private val DamenMarineBlue100 = Color(0xFF58678C)
private val DamenMarineBlue200 = Color(0xFF4A5B83)
private val DamenMarineBlue300 = Color(0xFF3C4F7B)
private val DamenMarineBlue400 = Color(0xFF354773)
private val DamenMarineBlue500 = Color(0xFF2D3E68)
private val DamenMarineBlue600 = Color(0xFF27355B)
private val DamenMarineBlue700 = Color(0xFF1D2543)
val DamenMarineBlack = Color(0xFF011736)
val DamenSilverBlue = Color(0xFFF2F6F8)
val DamenGreenLight = Color(0xFF24D18B)
val DamenGreenDark = Color(0xFF2B8360)
val DamenRedLight = Color(0xFFFF3B3B)
val DamenRedDark = Color(0xFF880F0F)
val DamenYellowLight = Color(0xFFFFE072)
val DamenYellowDark = Color(0xFFCDB998)
val DamenWhite = Color.White
val DamenSilver = TonedColor(
	lightest = DamenSilver100,
	lighter = DamenSilver200,
	light = DamenSilver300,
	medium = DamenSilver400,
	dark = DamenSilver500,
	darker = DamenSilver600,
	darkest = DamenSilver700,
	default = DamenSilverBlue,
)
val DamenBlue = TonedColor(
	lightest = DamenBlue100,
	lighter = DamenBlue200,
	light = DamenBlue300,
	medium = DamenBlue400,
	dark = DamenBlue500,
	darker = DamenBlue600,
	darkest = DamenBlue700,
	default = DamenBlue500,
)
val DamenOrange = TonedColor(
	lightest = DamenOrange100,
	lighter = DamenOrange200,
	light = DamenOrange300,
	medium = DamenOrange400,
	dark = DamenOrange500,
	darker = DamenOrange600,
	darkest = DamenOrange700,
	default = DamenOrange400,
)
val DamenLightBlue = TonedColor(
	lightest = DamenLightBlue100,
	lighter = DamenLightBlue200,
	light = DamenLightBlue300,
	medium = DamenLightBlue400,
	dark = DamenLightBlue500,
	darker = DamenLightBlue600,
	darkest = DamenLightBlue700,
	default = DamenLightBlue300,
)
val DamenMarineBlue = TonedColor(
	lightest = DamenMarineBlue100,
	lighter = DamenMarineBlue200,
	light = DamenMarineBlue300,
	medium = DamenMarineBlue400,
	dark = DamenMarineBlue500,
	darker = DamenMarineBlue600,
	darkest = DamenMarineBlue700,
	default = DamenMarineBlue600,
)
val OldDamenDarkGray = Color(0xFF181A1B)
val OldDamenDarkBlue = Color(0xFF051D4B)
val OldDamenLightBlue = Color(0xFF28458E)
val OldDamenOrange = Color(0xFFC34E06)
val OldDamenLightGray = Color(0xFF9D9488)
val OldDamenMediumGray = Color(0xFF303436)
val OldDamenMiddleGray = Color(0xFF1A1A1A)
val OldDamenSideGray = Color(0xFF333333)
val OldDamenAnthracite = Color(0xFF0C0C0C)
val OldHalfLifeOrange = Color(0XFFFB7E14)
val OldDamenWater = Color(0xFF6075AB)
val OldSelectedColour = OldHalfLifeOrange
val SelectedColour = OldHalfLifeOrange

data class TonedColor(
	val lightest: Color,
	val lighter: Color,
	val light: Color,
	val medium: Color,
	val dark: Color,
	val darker: Color,
	val darkest: Color,
	val default: Color,
) {
	operator fun invoke() = default
}

enum class DamenColor(val tones: TonedColor) {
	SilverBlue(DamenSilver),
	Blue(DamenBlue),
	Orange(DamenOrange),
	LightBlue(DamenLightBlue),
	MarineBlue(DamenMarineBlue),
}

fun getDamenPalette(
	n: Int,
	color: DamenColor = DamenColor.Orange,
): List<Color> {
	require(n <= 7)
	return when (n) {
		in Int.MIN_VALUE until 0 -> throw IllegalArgumentException()
		0 -> listOf(DamenRedLight)
		1 -> listOf(color.tones.default)
		2 -> listOf(color.tones.lighter, color.tones.medium)
		3 -> listOf(color.tones.lighter, color.tones.medium, color.tones.darker)
		4 -> listOf(color.tones.lightest, color.tones.light, color.tones.dark, color.tones.darkest)
		5 -> listOf(
			color.tones.lighter,
			color.tones.light,
			color.tones.medium,
			color.tones.dark,
			color.tones.darker,
		)
		6 -> listOf(
			color.tones.lighter,
			color.tones.light,
			color.tones.medium,
			color.tones.dark,
			color.tones.darker,
			color.tones.darkest,
		)
		7 -> listOf(
			color.tones.lightest,
			color.tones.lighter,
			color.tones.light,
			color.tones.medium,
			color.tones.dark,
			color.tones.darker,
			color.tones.darkest,
		)
		else -> throw IllegalArgumentException()
	}
}



