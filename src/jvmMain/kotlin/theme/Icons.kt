package view.theme

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.materialIcon
import androidx.compose.material.icons.materialPath
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.PathFillType
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.graphics.vector.PathBuilder
import kotlin.math.PI
import kotlin.math.cos
import kotlin.math.sin

// !!!IMPORTANT: INTERNAL CANVAS IS 24x24 WITH TopLeft = (0,0)
val Icons.Sharp.Square: ImageVector
	get() {
		if (_square != null) {
			return _square!!
		}
		_square = materialIcon(name = "Sharp.Square") {
			materialPath(pathFillType = PathFillType.EvenOdd) {
				moveTo(0f, 0f)
				horizontalLineTo(24f)
				verticalLineTo(24f)
				horizontalLineTo(0f)
				verticalLineTo(0f)
				close()
			}
		}
		return _square!!
	}
private var _square: ImageVector? = null
val Icons.TwoTone.Minus: ImageVector
	get() {
		if (_minus != null) {
			return _minus!!
		}
		_minus = materialIcon(name = "TwoTone.Minus") {
			materialPath {
				moveTo(19.0f, 13.0f)
				horizontalLineToRelative(-14f)
				verticalLineToRelative(-2f)
				horizontalLineTo(19f)
				verticalLineTo(13f)
				close()
			}
		}
		return _minus!!
	}
private var _minus: ImageVector? = null
val Icons.TwoTone.PieChart: ImageVector
	get() {
		if (_pieChart != null) {
			return _pieChart!!
		}
		_pieChart = materialIcon(name = "TwoTone.PieChart") {
			val firstAngle = 60f
			val secondAngle = 360f - firstAngle
			val radius = 12f
			fun PathBuilder.inner() {
				val innerRadius = 11f
				moveTo(12f, 12f)
				lineTo(12f, 12f - innerRadius)
				arcTo(
					horizontalEllipseRadius = innerRadius,
					verticalEllipseRadius = innerRadius,
					theta = firstAngle,
					isMoreThanHalf = firstAngle > 180f,
					isPositiveArc = true,
					x1 = 12f + innerRadius * cos((-90f + firstAngle) / 180f * PI).toFloat(),
					y1 = 12f + innerRadius * sin((-90f + firstAngle) / 180f * PI).toFloat(),
				)
				lineTo(12f, 12f)
				close()
			}
			drawWithInner(
				inner = PathBuilder::inner
			) {
				moveTo(12f, 12f)
				lineTo(12f, 12f - radius)
				arcTo(
					horizontalEllipseRadius = radius,
					verticalEllipseRadius = radius,
					theta = firstAngle,
					isMoreThanHalf = false,
					isPositiveArc = true,
					x1 = 12f + radius * cos((-90f + firstAngle) / 180f * PI).toFloat(),
					y1 = 12f + radius * sin((-90f + firstAngle) / 180f * PI).toFloat(),
				)
				lineTo(12f, 12f)
				close()
				moveTo(12f, 12f)
				lineTo(
					12f + radius * cos((-90f + firstAngle) / 180f * PI).toFloat(),
					12f + radius * sin((-90f + firstAngle) / 180f * PI).toFloat(),
				)
				arcTo(
					horizontalEllipseRadius = radius,
					verticalEllipseRadius = radius,
					theta = secondAngle,
					isMoreThanHalf = true,
					isPositiveArc = true,
					x1 = 12f + radius * cos((-90f + firstAngle + secondAngle) / 180f * PI).toFloat(),
					y1 = 12f + radius * sin((-90f + firstAngle + secondAngle) / 180f * PI).toFloat(),
				)
				lineTo(12f, 12f)
				close()
			}
		}
		return _pieChart!!
	}
private var _pieChart: ImageVector? = null
val Icons.TwoTone.BarChart: ImageVector
	get() {
		val goldenRatio = 1.618034f
		val outer = 6.6666665f
		val inner = outer - 2f
		if (_barChart != null) {
			return _barChart!!
		}
		fun PathBuilder.inner() {
			moveTo(2f, 22f)
			horizontalLineToRelative(inner)
			verticalLineToRelative(-((22f / goldenRatio) / goldenRatio - 2f))
			horizontalLineToRelative(-inner)
			close()
			moveTo(8f + 2f * inner, 22f)
			horizontalLineToRelative(inner)
			verticalLineToRelative(-(22f / goldenRatio - 2f))
			horizontalLineToRelative(-inner)
			close()
		}
		_barChart = materialIcon(name = "TwoTone.BarChart") {
			drawWithInner(
				inner = PathBuilder::inner
			) {
				moveTo(1f, 23f)
				horizontalLineToRelative(outer)
				verticalLineToRelative(-((22f / goldenRatio) / goldenRatio))
				horizontalLineToRelative(-outer)
				close()
				moveTo(2f + outer, 23f)
				horizontalLineToRelative(outer)
				verticalLineTo(2f)
				horizontalLineToRelative(-outer)
				close()
				moveTo(3f + 2 * outer, 23f)
				horizontalLineToRelative(outer)
				verticalLineToRelative(-(22f / goldenRatio))
				horizontalLineToRelative(-outer)
				close()
			}
		}
		return _barChart!!
	}
private var _barChart: ImageVector? = null
val Icons.TwoTone.DialChart: ImageVector
	get() {
		if (_dialChart != null) {
			return _dialChart!!
		}
		fun PathBuilder.inner() {
			val outerRadius = 11f
			val innerRadius = outerRadius - 5f
			val topLeft = Offset(
				12f + outerRadius * cos(-65f / 180f * PI).toFloat(),
				18f + outerRadius * sin(-65f / 180f * PI).toFloat(),
			)
			val botLeft = Offset(
				12f + innerRadius * cos(-65f / 180f * PI).toFloat(),
				18f + innerRadius * sin(-65f / 180f * PI).toFloat(),
			)
			val botRight = Offset(
				12f + innerRadius * cos(-10f / 180f * PI).toFloat(),
				18f + innerRadius * sin(-10f / 180f * PI).toFloat(),
			)
			val topRight = Offset(
				12f + outerRadius * cos(-5f / 180f * PI).toFloat(),
				18f + outerRadius * sin(-5f / 180f * PI).toFloat(),
			)
			moveTo(botLeft.x, botLeft.y)
			lineTo(topLeft.x, topLeft.y)
			arcTo(
				horizontalEllipseRadius = outerRadius,
				verticalEllipseRadius = outerRadius,
				theta = 90f,
				isMoreThanHalf = false,
				isPositiveArc = true,
				x1 = topRight.x,
				y1 = topRight.y,
			)
			lineTo(botRight.x, botRight.y)
			arcTo(
				horizontalEllipseRadius = innerRadius,
				verticalEllipseRadius = innerRadius,
				theta = 180f,
				isMoreThanHalf = false,
				isPositiveArc = false,
				x1 = botLeft.x,
				y1 = botLeft.y,
			)
			close()
		}
		_dialChart = materialIcon(name = "TwoTone.DialChart") {
			drawWithInner(
				inner = PathBuilder::inner
			) {
				val outerRadius = 12f
				val innerRadius = outerRadius - 7f
				moveTo(7f, 18f)
				horizontalLineTo(0f)
				arcTo(
					horizontalEllipseRadius = outerRadius,
					verticalEllipseRadius = outerRadius,
					theta = 180f,
					isMoreThanHalf = false,
					isPositiveArc = true,
					x1 = 24f,
					y1 = 18f,
				)
				horizontalLineTo(17f)
				arcTo(
					horizontalEllipseRadius = innerRadius,
					verticalEllipseRadius = innerRadius,
					theta = 180f,
					isMoreThanHalf = false,
					isPositiveArc = false,
					x1 = 7f,
					y1 = 18f,
				)
				close()
			}
		}
		return _dialChart!!
	}
private var _dialChart: ImageVector? = null
val Icons.TwoTone.Table: ImageVector
	get() {
		if (_table != null) {
			return _table!!
		}
		_table = materialIcon(name = "TwoTone.Table") {
			fun PathBuilder.inner() {
				val innerUnit = (24f - 2f * 3f - 2f) / 3f
				repeat(3) { x ->
					repeat(2) { y ->
						moveTo(1f + x * (3f + innerUnit), 1f + (y + 1) * (3f + innerUnit))
						horizontalLineToRelative(innerUnit)
						verticalLineToRelative(innerUnit)
						horizontalLineToRelative(-innerUnit)
						verticalLineToRelative(-innerUnit)
						close()
					}
				}
			}
			drawWithInner(
				inner = PathBuilder::inner
			) {
				val outerUnit = (24f - 2f * 1f) / 3f
				repeat(3) { x ->
					repeat(3) { y ->
						moveTo(x * (1f + outerUnit), y * (1f + outerUnit))
						horizontalLineToRelative(outerUnit)
						verticalLineToRelative(outerUnit)
						horizontalLineToRelative(-outerUnit)
						verticalLineToRelative(-outerUnit)
						close()
					}
				}
			}
		}
		return _table!!
	}
private var _table: ImageVector? = null
val Icons.TwoTone.Text: ImageVector
	get() {
		if (_text != null) {
			return _text!!
		}
		_text = materialIcon(name = "TwoTone.Text") {
			fun PathBuilder.inner() {
				moveTo(1f, 1f)
				horizontalLineTo(23f)
				verticalLineTo(23f)
				horizontalLineTo(1f)
				verticalLineTo(1f)
				close()
				moveTo(3f, 3f)
				horizontalLineTo(21f)
				verticalLineToRelative(4f)
				lineToRelative(-2f, -2f)
				horizontalLineTo(13f)
				verticalLineTo(19f)
				lineToRelative(2f, 2f)
				horizontalLineToRelative(-6f)
				lineToRelative(2f, -2f)
				verticalLineTo(5f)
				horizontalLineTo(5f)
				lineToRelative(-2f, 2f)
				close()
			}
			drawWithInner(
				inner = PathBuilder::inner,
			) {
				moveTo(0f, 0f)
				horizontalLineTo(25f)
				verticalLineTo(25f)
				horizontalLineTo(0f)
				verticalLineTo(0f)
				close()
			}
		}
		return _text!!
	}
private var _text: ImageVector? = null
val Icons.TwoTone.ListView: ImageVector
	get() {
		if (_listView != null) {
			return _listView!!
		}
		val ys = listOf(0f, 8f, 14f, 20f)
		_listView = materialIcon(name = "TwoTone.ListView") {
			fun PathBuilder.inner() {
				ys.map { it + 1f }.forEach {
					moveTo(1f, it)
					horizontalLineToRelative(2f)
					verticalLineToRelative(2f)
					horizontalLineToRelative(-2f)
					close()
					moveTo(7f, it)
					horizontalLineTo(23f)
					verticalLineToRelative(2f)
					horizontalLineTo(7f)
					close()
				}
			}
			drawWithInner(
				inner = PathBuilder::inner,
			) {
				ys.forEach {
					moveTo(0f, it)
					horizontalLineToRelative(4f)
					verticalLineToRelative(4f)
					horizontalLineToRelative(-4f)
					close()
					moveTo(6f, it)
					horizontalLineTo(24f)
					verticalLineToRelative(4f)
					horizontalLineTo(6f)
					close()
				}
				moveTo(1f, 5.5f)
				horizontalLineTo(23f)
				verticalLineToRelative(1f)
				horizontalLineTo(1f)
				close()
			}
		}
		return _listView!!
	}
private var _listView: ImageVector? = null
//val Icons.TwoTone.ListView: ImageVector
//	get() {
//		if (_listView != null) {
//			return _listView!!
//		}
//		val ys = listOf(1f, 7f, 11f, 15f, 19f)
//		_listView = materialIcon(name = "TwoTone.ListView") {
//			fun PathBuilder.inner() {
//				ys.forEach {
//					moveTo(2f, it + 1f)
//					horizontalLineToRelative(1f)
//					verticalLineToRelative(1f)
//					horizontalLineToRelative(-1f)
//					verticalLineToRelative(-1f)
//					close()
//				}
//				moveTo(6f, 2f)
//				horizontalLineToRelative(7f)
//				verticalLineToRelative(1f)
//				horizontalLineToRelative(-7f)
//				verticalLineToRelative(-1f)
//				close()
//				moveTo(16f, 2f)
//				horizontalLineToRelative(6f)
//				verticalLineToRelative(1f)
//				horizontalLineToRelative(-6f)
//				verticalLineToRelative(-1f)
//				close()
//			}
//			drawWithInner(
//				inner = PathBuilder::inner,
//			) {
//				ys.forEach {
//					moveTo(1f, it)
//					horizontalLineToRelative(3f)
//					verticalLineToRelative(3f)
//					horizontalLineToRelative(-3f)
//					verticalLineToRelative(-3f)
//					close()
//					moveTo(5f, 1f)
//					horizontalLineToRelative(9f)
//					verticalLineToRelative(3f)
//					horizontalLineToRelative(-9f)
//					verticalLineToRelative(-3f)
//					close()
//					moveTo(15f, 1f)
//					horizontalLineToRelative(8f)
//					verticalLineToRelative(3f)
//					horizontalLineToRelative(-8f)
//					verticalLineToRelative(-3f)
//					close()
//					moveTo(1f, 5f)
//					horizontalLineTo(23f)
//					verticalLineTo(6f)
//					horizontalLineTo(1f)
//					verticalLineTo(5f)
//					close()
//					if (it != 1f) {
//						moveTo(6f, it+1)
//						horizontalLineToRelative(7f)
//						verticalLineToRelative(1f)
//						horizontalLineToRelative(-7f)
//						verticalLineToRelative(-1f)
//						close()
//						moveTo(16f, it+1)
//						horizontalLineToRelative(6f)
//						verticalLineToRelative(1f)
//						horizontalLineToRelative(-6f)
//						verticalLineToRelative(-1f)
//						close()
//					}
//				}
//			}
//		}
//		return _listView!!
//	}
//private var _listView: ImageVector? = null
fun ImageVector.Builder.drawWithInner(
	inner: PathBuilder.() -> Unit,
	outer: PathBuilder.() -> Unit,
): ImageVector.Builder {
	materialPath(fillAlpha = 0.3f, strokeAlpha = 0.3f) {
		inner()
	}
	materialPath(pathFillType = PathFillType.EvenOdd) {
		inner()
		outer()
	}
	return this
}