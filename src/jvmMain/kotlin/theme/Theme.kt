package view.theme

import androidx.compose.material.Colors
import androidx.compose.ui.graphics.Color

fun damenBlue(
	primary: Color = DamenWhite,
	primaryVariant: Color = DamenLightBlue(),
	secondary: Color = DamenOrange(),
	secondaryVariant: Color = DamenOrange.darker,
	background: Color = DamenBlue(),
	surface: Color = DamenBlue(),
	error: Color = DamenRedLight,
	onPrimary: Color = DamenMarineBlack,
	onSecondary: Color = DamenMarineBlack,
	onBackground: Color = DamenWhite,
	onSurface: Color = DamenWhite,
	onError: Color = DamenMarineBlack,
): Colors = Colors(
	primary = primary,
	primaryVariant = primaryVariant,
	secondary = secondary,
	secondaryVariant = secondaryVariant,
	background = background,
	surface = surface,
	error = error,
	onPrimary = onPrimary,
	onSecondary = onSecondary,
	onBackground = onBackground,
	onSurface = onSurface,
	onError = onError,
	isLight = false
)

fun damenLight(
	primary: Color = DamenMarineBlack,
	primaryVariant: Color = DamenMarineBlue(),
	secondary: Color = DamenOrange(),
	secondaryVariant: Color = DamenOrange.darker,
	background: Color = DamenLightBlue(),
	surface: Color = DamenLightBlue(),
	error: Color = DamenRedDark,
	onPrimary: Color = DamenWhite,
	onSecondary: Color = DamenWhite,
	onBackground: Color = DamenMarineBlack,
	onSurface: Color = DamenMarineBlack,
	onError: Color = DamenWhite,
): Colors = Colors(
	primary = primary,
	primaryVariant = primaryVariant,
	secondary = secondary,
	secondaryVariant = secondaryVariant,
	background = background,
	surface = surface,
	error = error,
	onPrimary = onPrimary,
	onSecondary = onSecondary,
	onBackground = onBackground,
	onSurface = onSurface,
	onError = onError,
	isLight = true
)

fun damenWhite(
	primary: Color = DamenMarineBlack,
	primaryVariant: Color = DamenMarineBlue(),
	secondary: Color = DamenOrange(),
	secondaryVariant: Color = DamenOrange.darker,
	background: Color = DamenWhite,
	surface: Color = DamenWhite,
	error: Color = DamenRedDark,
	onPrimary: Color = DamenWhite,
	onSecondary: Color = DamenWhite,
	onBackground: Color = DamenMarineBlack,
	onSurface: Color = DamenMarineBlack,
	onError: Color = DamenWhite,
): Colors = Colors(
	primary = primary,
	primaryVariant = primaryVariant,
	secondary = secondary,
	secondaryVariant = secondaryVariant,
	background = background,
	surface = surface,
	error = error,
	onPrimary = onPrimary,
	onSecondary = onSecondary,
	onBackground = onBackground,
	onSurface = onSurface,
	onError = onError,
	isLight = true
)

//It explicitly states in the Style Guide this is not allowed,
// but I think it looks the best in this circumstance
fun damenIllegal(
	primary: Color = DamenOrange(),
	primaryVariant: Color = DamenOrange.darker,
	secondary: Color = DamenLightBlue(),
	secondaryVariant: Color = DamenLightBlue.darker,
	background: Color = DamenMarineBlack,
	surface: Color = DamenMarineBlack,
	error: Color = DamenRedDark,
	onPrimary: Color = DamenMarineBlue(),
	onSecondary: Color = DamenMarineBlue(),
	onBackground: Color = DamenLightBlue(),
	onSurface: Color = DamenLightBlue(),
	onError: Color = DamenMarineBlue(),
): Colors = Colors(
	primary = primary,
	primaryVariant = primaryVariant,
	secondary = secondary,
	secondaryVariant = secondaryVariant,
	background = background,
	surface = surface,
	error = error,
	onPrimary = onPrimary,
	onSecondary = onSecondary,
	onBackground = onBackground,
	onSurface = onSurface,
	onError = onError,
	isLight = true
)


