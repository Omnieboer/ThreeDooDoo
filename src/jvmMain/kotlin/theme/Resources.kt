package view.theme

import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Paint
import androidx.compose.ui.res.useResource
import org.jetbrains.skia.Font
import org.jetbrains.skia.Typeface
import org.jetbrains.skia.makeFromFile
import kotlin.io.path.createTempFile
import kotlin.io.path.outputStream
import kotlin.io.path.pathString

object Resources {
	fun color(a: Int = 255, r: Int, g: Int, b: Int) = Paint().asFrameworkPaint().setARGB(a, r, g, b)
	fun color(color: Color) = color(
		(color.alpha * 255).toInt(),
		(color.red * 255).toInt(),
		(color.blue * 255).toInt(),
		(color.red * 255).toInt()
	)
	val jbMono: Font

	init {
		val tempFile = createTempFile()
		useResource("jbMono.ttf") {
			it.use { input ->
				tempFile.outputStream().use { output ->
					input.copyTo(output)
				}
			}
		}
		jbMono = Font(Typeface.makeFromFile(tempFile.pathString), 16f)
	}
}